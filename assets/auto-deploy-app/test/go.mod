module gitlab.com/gitlab-org/charts/auto-deploy-app/test

go 1.15

require (
	github.com/gruntwork-io/terratest v0.32.1
	github.com/stretchr/testify v1.6.1
	k8s.io/api v0.19.7
	k8s.io/apimachinery v0.19.7
)
